package com.example.simplekotlinmvp.ui.home

import android.view.View
import com.example.simplekotlinmvp.ui.base.BaseView

/**
 * Created by sally on 6/19/17.
 */
interface HomeView : BaseView {
    fun dal(view: View)
    fun kho(view: View)
    fun ha(view: View)
    fun jim(view: View)
    fun tsa(view: View)
    fun ta(view: View)
    fun ba(view: View)
    fun alif(view: View)
    fun tho(view: View)
    fun dhod(view: View)
    fun shod(view: View)
    fun sin(view: View)
    fun syin(view: View)
    fun zay(view: View)
    fun ro(view: View)
    fun dzal(view: View)
    fun mim(view: View)
    fun lam(view: View)
    fun kaf(view: View)
    fun qof(view: View)
    fun fa(view: View)
    fun ghoin(view: View)
    fun ain(view: View)
    fun dzo(view: View)
    fun ya(view: View)
    fun hamzah(view: View)
    fun lamAlif(view: View)
    fun haa(view: View)
    fun waw(view: View)
    fun nun(view: View)

}