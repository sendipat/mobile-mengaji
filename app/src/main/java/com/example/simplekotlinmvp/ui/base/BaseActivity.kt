package com.example.simplekotlinmvp.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager

/**
 * Created by sally on 6/19/17.
 */
abstract class BaseActivity : AppCompatActivity(), BaseView {
    open var presenter: BasePresenter<*>? = null

    protected abstract fun initializeDagger()

    protected abstract fun initializePresenter()

    protected abstract fun initialize(bundle: Bundle?)

    abstract var layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(layoutId)



        initializeDagger()
        initializePresenter()

        initialize(intent!!.extras)
    }
}