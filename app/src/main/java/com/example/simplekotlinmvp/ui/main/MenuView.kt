package com.example.simplekotlinmvp.ui.main

import android.app.Activity
import android.view.View
import com.example.simplekotlinmvp.ui.base.BaseView

interface MenuView:BaseView {
    fun next(view: View)
}