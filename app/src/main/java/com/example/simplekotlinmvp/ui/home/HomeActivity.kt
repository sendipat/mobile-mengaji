package com.example.simplekotlinmvp.ui.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.example.simplekotlinmvp.R
import com.example.simplekotlinmvp.SimpleKotlinApp
import com.example.simplekotlinmvp.ui.base.BaseActivity
import javax.inject.Inject
import android.view.animation.AnimationUtils
import android.media.MediaPlayer
import android.view.View
import android.widget.ImageButton


/**
 * Created by sally on 6/19/17.
 */
class HomeActivity : BaseActivity(), HomeView {
    override fun initialize(bundle: Bundle?) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    @Inject
    lateinit var homePresenter : HomePresenter

    companion object {
        fun open(activity: Activity) {
            val intent = Intent(activity, HomeActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun initializePresenter() {
        super.presenter = homePresenter
        homePresenter.view = this
    }



    override fun dal(view: View) {
        val mp = MediaPlayer.create(this, R.raw.dal)

        val button = findViewById(R.id.actionDal) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun kho(view: View) {
        val mp = MediaPlayer.create(this, R.raw.kho)

        val button = findViewById(R.id.actionKho) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun ha(view: View) {
        val mp = MediaPlayer.create(this, R.raw.ha)

        val button = findViewById(R.id.actionHa) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun jim(view: View) {
        val mp = MediaPlayer.create(this, R.raw.jim)

        val button = findViewById(R.id.actionJim) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun tsa(view: View) {
        val mp = MediaPlayer.create(this, R.raw.tsa)

        val button = findViewById(R.id.actionTsa) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun ta(view: View) {
        val mp = MediaPlayer.create(this, R.raw.ta)

        val button = findViewById(R.id.actionTa) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun ba(view: View) {
        val mp = MediaPlayer.create(this, R.raw.ba)

        val button = findViewById(R.id.actionBa) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun alif(view: View) {
        val mp = MediaPlayer.create(this, R.raw.alif)

        val button = findViewById(R.id.actionAlif) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun tho(view: View) {
        val mp = MediaPlayer.create(this, R.raw.tho)

        val button = findViewById(R.id.actionTho) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun dhod(view: View) {
        val mp = MediaPlayer.create(this, R.raw.dhod)

        val button = findViewById(R.id.actionDhod) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun shod(view: View) {
        val mp = MediaPlayer.create(this, R.raw.shod)

        val button = findViewById(R.id.actionShod) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun syin(view: View) {
        val mp = MediaPlayer.create(this, R.raw.syin)

        val button = findViewById(R.id.actionSyin) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun sin(view: View) {
        val mp = MediaPlayer.create(this, R.raw.sin)

        val button = findViewById(R.id.actionSin) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun zay(view: View) {
        val mp = MediaPlayer.create(this, R.raw.za)

        val button = findViewById(R.id.actionZay) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun ro(view: View) {
        val mp = MediaPlayer.create(this, R.raw.ro)

        val button = findViewById(R.id.actionRo) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun dzal(view: View) {
        val mp = MediaPlayer.create(this, R.raw.dzal)

        val button = findViewById(R.id.actionDzal) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun mim(view: View) {
        val mp = MediaPlayer.create(this, R.raw.mim)

        val button = findViewById(R.id.actionMim) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun lam(view: View) {
        val mp = MediaPlayer.create(this, R.raw.lam)

        val button = findViewById(R.id.actionLam) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun kaf(view: View) {
        val mp = MediaPlayer.create(this, R.raw.kaf)

        val button = findViewById(R.id.actionKaf) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun qof(view: View) {
        val mp = MediaPlayer.create(this, R.raw.qof)

        val button = findViewById(R.id.actionQof) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }


    override fun fa(view: View) {
        val mp = MediaPlayer.create(this, R.raw.fa)

        val button = findViewById(R.id.actionFa) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun ghoin(view: View) {
        val mp = MediaPlayer.create(this, R.raw.ghain)

        val button = findViewById(R.id.actionGhoin) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }

    override fun ain(view: View) {
        val mp = MediaPlayer.create(this, R.raw.ain)

        val button = findViewById(R.id.actionAin) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun dzo(view: View) {
        val mp = MediaPlayer.create(this, R.raw.dzo)

        val button = findViewById(R.id.actionDzo) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun ya(view: View) {
        val mp = MediaPlayer.create(this, R.raw.ya)

        val button = findViewById(R.id.actionYa) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun hamzah(view: View) {
        val mp = MediaPlayer.create(this, R.raw.hamzah)

        val button = findViewById(R.id.actionHamzah) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun lamAlif(view: View) {
        val mp = MediaPlayer.create(this, R.raw.lam_alif)

        val button = findViewById(R.id.actionLamAlif) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun haa(view: View) {
        val mp = MediaPlayer.create(this, R.raw.ha)

        val button = findViewById(R.id.actionHaa) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun waw(view: View) {
        val mp = MediaPlayer.create(this, R.raw.waw)

        val button = findViewById(R.id.actionWaw) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }
    override fun nun(view: View) {
        val mp = MediaPlayer.create(this, R.raw.nun)

        val button = findViewById(R.id.actionNun) as ImageButton
        val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
        button.startAnimation(myAnim)
        mp.start()
        mp.setOnCompletionListener { mp.release() }

    }


    override fun initializeDagger() {
        val app = application as SimpleKotlinApp
        app.appComponent?.inject(this)
    }

    override var layoutId: Int = R.layout.action_hijaiyah

}