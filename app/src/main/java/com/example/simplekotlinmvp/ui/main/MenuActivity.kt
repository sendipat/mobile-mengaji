package com.example.simplekotlinmvp.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import com.example.simplekotlinmvp.R
import com.example.simplekotlinmvp.SimpleKotlinApp
import com.example.simplekotlinmvp.ui.base.BaseActivity
import com.example.simplekotlinmvp.ui.home.HomeActivity
import javax.inject.Inject

class MenuActivity: BaseActivity(), MenuView {
    override fun initialize(bundle: Bundle?) {
        }

    @Inject
    lateinit var menuPresenter: MenuPresenter

    companion object {
        fun open(activity: Activity) {
            val intent = Intent(activity, MenuActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun initializePresenter() {
        super.presenter = menuPresenter
        menuPresenter.view = this
    }

    override fun initializeDagger() {
        val app = application as SimpleKotlinApp
        app.appComponent?.inject(this)
    }
    override fun next(view: View){
        val button = findViewById(R.id.actionNun) as Button
        button.setOnClickListener {
            startActivity(Intent(this@MenuActivity, HomeActivity::class.java))
        }
        }


    override var layoutId: Int = R.layout.activity_mengaji
}

