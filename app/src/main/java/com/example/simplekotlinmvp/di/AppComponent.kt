package com.example.simplekotlinmvp.di

import com.example.simplekotlinmvp.ui.home.HomeActivity
import com.example.simplekotlinmvp.ui.main.MenuActivity
import com.example.simplekotlinmvp.ui.splash.SplashActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by sally on 6/19/17.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun inject(splashActivity: SplashActivity)
    fun inject(homeActivity: HomeActivity)
    fun inject(menuActivity: MenuActivity)
}
